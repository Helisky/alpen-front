import Vue from 'vue';
import VueRouter from 'vue-router';

// import store from '@/store'

Vue.use(VueRouter);

function configRoutes() {
	return [
		{
			path: '/',
			redirect: '/dashboard',
			name: 'Home',
			component: () => import('@/views/Index'),
			children: [
				{
					name: 'dashboard',
					path: 'dashboard',
					component: () => import('@/views/Home')
				},
				{
					path: 'customers',
					redirect: '/customers/all',
					name: 'customers',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'customersAll',
							component: () => import('@/views/customer/List')
						}
					]
				},
				{
					path: 'providers',
					redirect: '/providers/all',
					name: 'providers',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'providersAll',
							component: () => import('@/views/provider/List')
						}
					]
				},
				{
					path: 'products',
					redirect: '/products/all',
					name: 'products',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'productosAll',
							component: () => import('@/views/products/List')
						}
					]
				},
				{
					path: 'categories',
					redirect: '/categories/all',
					name: 'category',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'categoriesAll',
							component: () => import('@/views/category/List')
						}
					]
				},
				{
					path: 'brands',
					redirect: '/brands/all',
					name: 'brand',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'brandAll',
							component: () => import('@/views/brand/List')
						}
					]
				},
				{
					path: 'units',
					redirect: '/units/all',
					name: 'unit',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'unitAll',
							component: () => import('@/views/unit/List')
						}
					]
				},
				{
					path: 'cash',
					redirect: '/cash/all',
					name: 'cash',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'cashAll',
							component: () => import('@/views/cash/Detail')
						}
					]
				},
				{
					path: 'sales',
					redirect: '/sales/all',
					name: 'sale',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'all',
							name: 'saleAll',
							component: () => import('@/views/sale/List')
						}
					]
				},
				{
					path: 'inventory',
					redirect: '/inventory/to',
					name: 'inventor',
					component: {
						render(c) {
							return c('router-view');
						}
					},
					children: [
						{
							path: 'to',
							name: 'invTo',
							component: () => import('@/views/inventory/Access')
						},
						{
							path: 'purchases',
							name: 'purchaseList',
							component: () => import('@/views/inventory/Purchases')
						}
					]
				}
			]
		}
	];
}

const router = new VueRouter({
	mode: 'hash',
	//   base: process.env.BASE_URL,
	routes: configRoutes(),
	scrollBehavior: () => ({ y: 0 })
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     if (!store.state.auth.status.loggedIn) {
//       next({
//         path: '/auth/login',
//       })
//     } else {
//       next()
//     }

//   } else {
//     next()
//   }
// })

export default router;
